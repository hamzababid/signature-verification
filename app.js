var express = require('express');
var app = express();
const fs = require('fs');
const mpgXAdES = require('mpg-xades');


app.get('/verify/signature', function (req, res) {
    let response = {hasError: false}
    try {
        const xml = fs.readFileSync('./pacs004_1636552152440.xml', 'utf8');
        console.log(xml)

        const publicKey = fs.readFileSync('./publicKey.txt', 'utf8');
        console.log(publicKey);


        const verified = mpgXAdES.verifyMXMessage(xml, publicKey);
        console.log(verified);

        if(verified && verified.valid)
            res.send( {...response, message: 'Success', data: verified});
        
        else 
            res.status(400).send({...response, message: 'Failure', data: verified})
    } catch (err) {
        console.error(err)
        res.status(400).send({...response, error: err});
    }
});
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});