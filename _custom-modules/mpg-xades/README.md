# mpg-xades


An xml digital signature library written after cloning https://www.npmjs.com/package/xml-crypto and customizing to fulfil the need for SBP Micro Payment Gateway integration. Here are SBP MPG requirements and business rules:

## 5.1.1 XAdES usage rules
XAdES-BES signatures used in the system should comply with the following usage
rules:
1. Usage of block “Object” (ds:Object)
The ds:Object element should have just one value in in QualifyingProperties/
SignedProperties/ SignedSignatureProperties: the signing time, specifying the
time at which the signer claims to have performed the signing process..
The Id attribute on the KeyInfo element is mandatory and the value of the ID
attribute must be an underscore (” _”) followed by a universally unique identifier
(UUID), that is either time-based or random.
2. Usage of block “KeyInfo”
The XAdES standard allows two different methods to comply with the XAdES-
BES requirement. In the system it has been decided to use the one that includes
the signer certificate in the KeyInfo element:
	* Element KeyInfo must be present and must include the
ds:X509Data/ds:X509Certificate/ ds:X509IssuerSerial containing the
issuer and the serial number of the signing certificate.
	* The Id attribute on the KeyInfo element is mandatory and the value of the
ID attribute must be an underscore (”_”) followed by a universally unique
identifier (UUID), that is either time-based or random.
	* The SignedInfo element must reference the KeyInfo element using the Id
attribute. Usage of the alternative ds:Object/ QualifyingProperties/
SignedProperties/ SignedSignatureProperties/ SigningCertificate element
is not allowed.

## 5.1.2 References in SignedInfo
There should be 3 references in SignedInfo:
1. Reference to KeyInfo (which contains unambiguous reference to the signer's
certificate). The reference should have attribute URI referencing Id of the KeyInfo
element.
2. Reference to SignedProperties under ds:Object. The reference should have
attribute URI referencing Id of the SignedProperties element.
3. Application-specific Reference without any URI – this Reference points to the
business document – element Document. Standard XML signatures allow a
special Reference: a Reference without URI attribute. It is defined by the XML
signature specification that the receiving application should be able to identify
which object to use in this case. In this specification and this System, the URI-
less Reference refers to the Document element with its entire contents.

## Install
Install with npm:

    npm install [path-to-module]mpg-xades

A pre requisite it to have [openssl](http://www.openssl.org/) installed and its /bin to be on the system path.

## Used Algorithms

### Canonicalization and Transformation Algorithms

* Exclusive Canonicalization http://www.w3.org/2001/10/xml-exc-c14n#

### Hashing Algorithms

* SHA256 digests http://www.w3.org/2001/04/xmlenc#sha256

### Signature Algorithms

* RSA-SHA1 http://www.w3.org/2000/09/xmldsig#rsa-sha1

## Signing Xml documents

In order to sign a xml document use `signMXMessage` method of `mpgXAdES` instance that takes below parameters:
- `XML` - **[required]** xml string to be signed
- `Certificate Issuer` - **[required]** Certificate issue name of signing party to be placed in KeyInfo element
- `Certificate Serial` - **[required]** Certificate serial number of signing party to be placed in KeyInfo element
- `Private Key` - **[required]** Private key string of signing party

Use this code:

`````javascript
    	const mpgXAdES = require('mpg-xades'), fs = require('fs')
	var xml = "<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pacs.008.001.08">" +
	            "<FIToFICstmrCdtTrf>" +
	              "...." +
	            "</FIToFICstmrCdtTrf>" +
	          "</Document>"
    	const privateKey = fs.readFileSync('ssl-certs/server.key');//replace with logic to retrieve private key string/buffer
	const responseObj = mpgXAdES.signMXMessage(xml, "C=SE, O=CMA Small Systems AB, CN=Test CA", "12345678", privateKey);
    	console.log(responseObj);// { success: boolean, signature: "", errorMessage: "" }

`````

The result will be:


`````javascript
	{ success: true,
  signature: '<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/><ds:Reference URI="#_7319609b-312d-4b83-b041-9a94fbdb1287"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>cotlF9MdB5P7UnmnXPvskgVEyoZATVoBidEjwYPXFmk=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903/v1.3.2#SignedProperties" URI="#_2438fa8a-ec37-4232-b416-0f66019b7f8d-signedprops"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>hQQdsLpu1zacjvXlVYh4o6/bCqt3bnxpxzoCXzkeT24=</ds:DigestValue></ds:Reference><ds:Reference><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>B0/NsyL1WzprThi4NJ/JedvhKWWk+HlhrdH7Q5ywJT0=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>Hf8Zet3NLrHMh4DhJHQoKJwo5hssiNcKcCbw4D6h1B/24gB7hCV+eeso/sWqvkNVXAj8PDJOqBMQkcWdRAETSYe+PFzgufFnAr8kT1Jh7tz7YwfTR7i23dT4GuNBBU7wxpp3AJopOo/y9UVJpVl+vmAQS9o9AupaSBFBpw1SgHZuDFELilRluucAlUyqHgJeRUl6bbp5hYtXjV6fbQR1lzUS6Qa8bn1Bn/VCp+cbhmk8czi0diRnw2adMnYwJ4h4M3fMcprRFAsZ/7jltNhFNIeNihn7uarTw+ao8HgEgCe9PWTQoAJEDzNvdGf34Hrn9otp1PLpU/hvwc862YnXlg==</ds:SignatureValue><ds:KeyInfo Id="_7319609b-312d-4b83-b041-9a94fbdb1287"><ds:X509Data><ds:X509IssuerSerial>\n\t  <ds:X509IssuerName>CN=HBL, OU=HBL</ds:X509IssuerName>\n\t  <ds:X509SerialNumber>232132131</ds:X509SerialNumber>\n\t  </ds:X509IssuerSerial></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#">\n              <xades:SignedProperties Id="_2438fa8a-ec37-4232-b416-0f66019b7f8d-signedprops">\n                <xades:SignedSignatureProperties>\n                  <xades:SigningTime>2020-07-02T10:10:34.957Z</xades:SigningTime>\n                </xades:SignedSignatureProperties>\n              </xades:SignedProperties>\n            </xades:QualifyingProperties></ds:Object></ds:Signature>',
  errorMessage: '' }
`````


## Verifying Xml documents

In order to verify signature of a signed xml document use `verifyMXMessage` method of `mpgXAdES` instance that takes below parameters:
- `XML` - **[required]** xml string with signature string 
- `Public Key` - **[required]** Public key string of signing party or certificate string

Example:

`````javascript
	const mpgXAdES = require('mpg-xades')
	var publicKey = "Retrieve public key";
	var xmlWithSignature = "signed xml"
    	const result = mpgXAdES.verifyMXMessage(xmlWithSignature, publicKey);
    	console.log(result);//{ valid: false, validationErrors: [], errorMessage: "" }
`````

if the verification process fails `validationErrors` will have the errors.

`````javascript
{ valid: false,
  validationErrors: 
   [ 'invalid signature: for uri null calculated digest is nkzgWo+v3tH1QV13HwoeBJjMuh9Hamp0e7e51BRytVY= but the xml to validate supplies digest B0/NsyL1WzprThi4NJ/JedvhKWWk+HlhrdH7Q5ywJT0=' ],
  errorMessage: 'Signature verification failed.' }
  `````

