const { v4: uuidv4 } = require('uuid');
var select = require('xpath').select;
const SignedXml = require('./lib/signed-xml').SignedXml;
const dom = require('xmldom').DOMParser;
function MXKeyInfo(id, certificateIssuer, certificateSerial) {
  this._id = id;
  this.getKeyInfo = function (key, prefix) {
    prefix = prefix || ''
    prefix = prefix ? prefix + ':' : prefix
    return `<${prefix}X509Data><${prefix}X509IssuerSerial>
	  <${prefix}X509IssuerName>${certificateIssuer}</${prefix}X509IssuerName>
	  <${prefix}X509SerialNumber>${certificateSerial}</${prefix}X509SerialNumber>
	  </${prefix}X509IssuerSerial></${prefix}X509Data>`
  }
}
function MXObjectInfo(id) {
  this._id = id;
  this.getObjectInfo = function (key, prefix) {
    prefix = prefix || ''
    prefix = prefix ? prefix + ':' : prefix
    return `<xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#">
              <xades:SignedProperties Id="${this._id}">
                <xades:SignedSignatureProperties>
                  <xades:SigningTime>${new Date().toISOString()}</xades:SigningTime>
                </xades:SignedSignatureProperties>
              </xades:SignedProperties>
            </xades:QualifyingProperties>`
  }
}
module.exports.signMXMessage = function (xml, certificateIssuer, certificateSerial, privateKey) {
  //- signMXMessage(xml, certificateIssuer, certificateSerial, privateKey) --- return { success: boolean, signature: "", errorMessage: "" }
  let responseObj = { success: false, signature: "", errorMessage: "" }
  try {
    var sig = new SignedXml()
    var keyInfoId = `_${uuidv4()}`
    var objectInfoId = `_${uuidv4()}-signedprops`
    //xpath, transforms, digestAlgorithm, uri, digestValue, inclusiveNamespacesPrefixList, isEmptyUri
    sig.addReference("//*[local-name(.)='KeyInfo']", null, "http://www.w3.org/2001/04/xmlenc#sha256", keyInfoId, null, null, false);
    sig.addReference("//*[local-name(.)='SignedProperties']", null, "http://www.w3.org/2001/04/xmlenc#sha256", objectInfoId, null, null, false, "http://uri.etsi.org/01903/v1.3.2#SignedProperties");
    sig.addReference("//*[local-name(.)='Document']", null, "http://www.w3.org/2001/04/xmlenc#sha256", null, null, null, true);
    sig.keyInfoProvider = new MXKeyInfo(keyInfoId, certificateIssuer, certificateSerial);
    sig.objectInfoProvider = new MXObjectInfo(objectInfoId);
    sig.signingKey = privateKey;
    sig.computeSignature(xml, { prefix: "ds" })
    responseObj.signature = sig.getSignatureXml();
    responseObj.success = true;
  }
  catch (err) {
    responseObj.errorMessage = err;
  }
  return responseObj;
}

function MXPublicKeyInfo(_publicKey) {
  this.publicKey = _publicKey;
  this.getKey = function (keyInfo) {
    return this.publicKey;
  }
}
module.exports.verifyMXMessage = function (xml, publicKey) {
  //- verifyMXMessage(xml, publicKey) -- { valid: false, validationErrors: [], errorMessage: "" }
  let responseObj = { valid: false, validationErrors: [], errorMessage: "" }
  try {
    var doc = new dom().parseFromString(xml)
    //var signature = select("//*[local-name(.)='Signature' and namespace-uri(.)='http://www.w3.org/2000/09/xmldsig#']", doc)[0]
    var signature = select("/*[name()='DataPDU']/*[name()='Body']/*[name()='AppHdr']/*[name()='Sgntr']/*[name()='ds:Signature' and namespace-uri(.)='http://www.w3.org/2000/09/xmldsig#']", doc)[0]
    
    var sig = new SignedXml()
    sig.keyInfoProvider = new MXPublicKeyInfo(publicKey);
    sig.loadSignature(signature)

    //responseObj.valid = sig.checkSignature(xml, "//*[local-name(.)='Document']")
    responseObj.valid = sig.checkSignature(xml, "/*[name()='DataPDU']/*[name()='Body']/*[name()='Document']")
    if (!responseObj.valid) {
      responseObj.errorMessage = "Signature verification failed.";
      responseObj.validationErrors = sig.validationErrors;
    }
  }
  catch (err) {
    responseObj.errorMessage = err;
  }

  return responseObj;
}